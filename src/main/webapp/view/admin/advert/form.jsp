<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
function fileUploadSuccess(file){
	$("#imgUrl").val(file);
}
</script>
</head>
<body>
    <form:form action="${path}/admin/advert/addOrUpdate" id="form_add_update" name="form_add_update" modelAttribute="pageForm">
       <form:hidden path="id" />
      <table class='formtable'>
        <tr>
          <td colspan="4"><div class="formtitle"><span>基本信息</span></div></td>
        </tr>
       <tr>
          <td>名称</td>
          <td><form:input path="title" cssClass="dfinput" datatype="required"  errormsg="不能为空"/></td>
          <td>所属广告位</td>
          <td>
              <form:select path="advertPositionId">
               <form:options items="${ADVERT_POSITION_LIST }" itemLabel="name" itemValue="id" />
              </form:select>
         </td>
       </tr>
       <tr>
          <td>图片链接</td>
          <td>
              <form:input path="imgUrl" cssClass="dfinput"/>
              <a href="javascript:fileUpload('fileUploadSuccess');" class="add">图片上传</a>
          </td>
          <td>链接地址</td>
          <td><form:input path="url" cssClass="dfinput"/></td>
       </tr>
       <tr>
          <td>开始时间</td>
          <td><form:input path="startTime" cssClass="dfinput" onclick="WdatePicker()"/></td>
          <td>结束时间</td>
          <td><form:input path="endTime" cssClass="dfinput" onclick="WdatePicker()"/></td>
       </tr>
       </table>
     </form:form>
</body>
</html>
