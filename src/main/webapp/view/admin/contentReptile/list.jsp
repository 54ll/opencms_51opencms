<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
</head>
<body>
    <form:form action="#" id="listForm" name="listForm">
    <jsp:include page="/view/admin/common/place.jsp" />
    <div class="rightinfo"  style="padding-left: 0;">
    <div class="tools">
    	<ul class="toolbar">
			  <li onclick="showMenu(this);"><span><i class="Hui-iconfont">&#xe600;</i></span>复制到</li>
        </ul>
    </div>
    <table class="tablelist">
       <thead>
	      <tr>
	        <th width="5%"><input type="checkbox"/></th>
	        <th width="10%">html索引号</th>
	        <th width="30%">信息标题</th>
	        <th width="10%">来源</th>
	        <th width="10%">作者</th>
	        <th width="10%">发布时间</th>
	        <th width="10%">创建时间</th>
            <th>操作</th>	      
          </tr>
       </thead>
        <tbody>
        <c:forEach items="${pageView.pageDate }" var="bean">
	        <tr>
	        <td><input name="id" type="checkbox" value="${bean.id }" /></td>
	        <td>${bean.id }</td>
	        <td>${bean.title}</td>
	        <td>${bean.source }</td>
	        <td>${bean.author }</td>
	        <td>${bean.publishTime }</td>
	        <td><fmt:formatDate value="${bean.createTime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
	        <td class="td-manage">
		        <a title="编辑" href="javascript:showForm('用户修改','${path}/admin/contentReptile/showForm?id=${bean.id }','100%','100%')" class="ml-5" style="text-decoration: none">
		            <i class="Hui-iconfont">&#xe6df;</i>
		        </a>
		        <a title="删除" href="javascript:del('${path }/admin/contentReptile/delete?id=${bean.id}')" class="ml-5" style="text-decoration: none">
		            <i class="Hui-iconfont">&#xe6e2;</i>
		        </a>
			</td>
	        </tr>
       </c:forEach>
        </tbody>
    </table>
    <jsp:include page="/view/admin/common/pageinfo.jsp" />
    </div>
    </form:form>
    <jsp:include page="/view/admin/contentReptile/channelTree.jsp"/>
</body>
</html>
