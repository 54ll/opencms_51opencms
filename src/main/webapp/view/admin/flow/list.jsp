<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript" src="${path }/view/admin/resource/js/flow.js"></script>
</head>
<body>
    <form:form action="#" id="listForm" name="listForm">
    <jsp:include page="/view/admin/common/place.jsp" />
    <div class="rightinfo">
    <div class="tools">
    	<ul class="toolbar">
	        <li onclick="showForm('用户添加','${path}/admin/flow/showForm','750','350')"><span><i class="Hui-iconfont">&#xe600;</i></span>添加</li>
        </ul>
    </div>
    <table class="tablelist">
    	<thead>
	    	<tr>
	        <th><input type="checkbox" checked="checked"/></th>
	        <th>编号</th>
	        <th>名称</th>
	        <th>是否开放</th>
	        <th>描述</th>
	        <th>排序</th>
         <th>操作</th>	        </tr>
       </thead>
        <tbody>
        <c:forEach items="${pageView.pageDate }" var="bean">
	        <tr onclick="getFlowFace(${bean.id},this)">
	        <td width="5%"><input name="id" type="checkbox" value="" /></td>
	        <td width="10%">${bean.id }</td>
	        <td width="10%">${bean.name }</td>
	        <td width="10%">
	           <c:choose>
	              <c:when test="${bean.isOpen==1 }">开启</c:when>
	              <c:otherwise>关闭</c:otherwise>
	           </c:choose>
	        </td>
	        <td width="10%">${bean.description }</td>
	        <td width="10%">${bean.sort }</td>
	        <td class="td-manage">
	            <a title="添加节点" href="javascript:showForm('添加节点','${path}/admin/flowFace/showForm?flow_id=${bean.id }','750','350')" class="ml-5" style="text-decoration: none">
		            <i class="Hui-iconfont">&#xe604;</i>
		        </a>
		        <a title="编辑" href="javascript:showForm('流程修改','${path}/admin/flow/showForm?id=${bean.id }','750','350')" class="ml-5" style="text-decoration: none">
		            <i class="Hui-iconfont">&#xe6df;</i>
		        </a>
		        <a title="删除" href="javascript:del('${path }/admin/flow/delete?id=${bean.id}')" class="ml-5" style="text-decoration: none">
		            <i class="Hui-iconfont">&#xe6e2;</i>
		        </a>
			</td>
	        </tr>
       </c:forEach>
        </tbody>
    </table>
   <jsp:include page="/view/admin/common/pageinfo.jsp" />
    </div>
    </form:form>
</body>
</html>
