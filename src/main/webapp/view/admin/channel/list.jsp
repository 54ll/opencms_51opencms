<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
</head>
<body>
    <form:form action="#" id="listForm" name="listForm">
    <input type="hidden" value="${pageView.pId }" id="parent_channel_id" name="pId"/>
    <div class="rightinfo" style="padding-top: 0;">
		<div class="tools">
	    	<ul class="toolbar">
		        <li onclick="showForm('用户添加','${path}/admin/channel/showForm?parent_channel_id=${pageView.id }','750','350')"><span><i class="Hui-iconfont">&#xe600;</i></span>添加</li>
	        </ul>
	    </div>
	    <table class="tablelist">
    	<thead>
	    	<tr>
	        <th><input type="checkbox" checked="checked"/></th>
	        <th>编号</th>
	        <th>父节点名称</th>
	        <th>名称</th>
	        <th>页面标示</th>
	        <th>是否导航</th>
	        <th>排序</th>
            <th>操作</th>	      
            </tr>
       </thead>
        <tbody>
        <c:forEach items="${pageView.pageDate }" var="bean">
            <c:if test="${bean.id!=1 }">
	        <tr>
	        <td><input name="id" type="checkbox" value="" /></td>
	        <td>${bean.id }</td>
	        <td>${bean.pName }</td>
	        <td>${bean.name }</td>
	        <td>${bean.code }</td>
	        <td>
		        <c:choose>
		           <c:when test="${bean.isNav==1 }">是</c:when>
		           <c:otherwise>否</c:otherwise>
		        </c:choose>
	        </td>
	        <td>${bean.sort }</td>
	        <td class="td-manage">
		        <a title="编辑" href="javascript:showForm('用户修改','${path}/admin/channel/showForm?id=${bean.id }','750','350')" class="ml-5" style="text-decoration: none">
		            <i class="Hui-iconfont">&#xe6df;</i>
		        </a>
		        <a title="删除" href="javascript:del('${path }/admin/channel/delete?id=${bean.id}')" class="ml-5" style="text-decoration: none">
		            <i class="Hui-iconfont">&#xe6e2;</i>
		        </a>
			</td>
	      </tr>
	      </c:if>
        </c:forEach>
        </tbody>
       </table>
       <jsp:include page="/view/admin/common/pageinfo.jsp" />
    </div>
    </form:form>
</body>
</html>
