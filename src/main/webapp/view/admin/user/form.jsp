<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
</head>
<body>
    <form:form id="form_add_update" name="form_add_update" modelAttribute="pageForm">
	    <div class="formbody">
		    <div class="formtitle"><span>基本信息</span></div>
		    <ul class="forminfo">
			    <li><label>姓名</label><form:input path="name" cssClass="dfinput"/></li>
			    <li><label>年龄</label><form:input path="age" cssClass="dfinput"/></li>
	            <li><label>&nbsp;</label><input type="button" class="btn" value="确认保存" onclick="addOrUpdate('form_add_update','${path}/admin/user/addOrUpdate')"/></li>
		    </ul>
	    </div>
     </form:form>
</body>
</html>
