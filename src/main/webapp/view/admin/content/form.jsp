<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fckeditor" uri="http://java.fckeditor.net" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
$(function(){
	var editor = UE.getEditor('contentText');
	$('#titleColor').cxColor();
	$('#weight').jRange({
		 from: 0, 
		 to: 100, 
		 step: 10, 
		 scale: [0,10,20,30,40,50,60,70,80,90,100], 
		 format: '%s', 
		 width: 600, 
		 showLabels: true, 
		 showScale: true,
		 isRange:false
	});
});
function fileUploadSuccess(file){
	$("#imgUrl").val(file);
	$("#imgShow").html('<img src='+file+' width="150" height="150" />');
}
function fileUploadSuccess1(file){
	var filePaths=file.split(";");
	var str="";
	for(var i=0;i<filePaths.length;i++){
		if(filePaths[i]){
			str+='<div style="width: 50%;height: 170px;float: left;">'+
			         '<div style="float: left;"><img src='+filePaths[i]+' width="150" height="150" style="margin: 10px" /></div>'+
				     '<div style="float: left;">'+
					     '图片地址:<input type="text" class="dfinput imagePath" name="imagePath" readonly value="'+filePaths[i]+'" /><br/>'+
					     '图片描述:<input type="text" class="dfinput imageName" name="imageName"  datatype="required"  errormsg="不能为空" /><br/>'+
					     '图片宽度:<input type="text" class="dfinput imageWidth" name="imageWidth" value="150"  datatype="required"  errormsg="不能为空" /><br/>'+
					     '图片高度:<input type="text" class="dfinput imageHeight" name="imageHeight" value="150"  datatype="required"  errormsg="不能为空" /><br/>'+
					     '<a href="javascript:void(0);" onclick="delImg(this)" class="delete">删除</a>'+
				     '</div>'+
			     '</div>';
		}
	}
    $("#imgsShow").append(str);
}
function fileUploadSuccess2(file){
	var filePaths=file.split(";");
	var str="";
	for(var i=0;i<filePaths.length;i++){
		if(filePaths[i]){
			str+='<div style="margin: 10px">'+
			             '附件描述:<input type="text" class="dfinput optionName" name="optionName" datatype="required"  errormsg="不能为空"  style="margin:0 30px 0 10px;" />'+
					     '附件地址:<input type="text" class="dfinput optionPath" name="optionPath" readonly value="'+filePaths[i]+'" style="margin:0 20px 0 10px;width:200px;" />'+
					     '<a href="javascript:void(0);" onclick="delOption(this)" class="delete">删除</a>'+
				 '</div>';
		}
	}
    $("#optionList").append(str);
}
function fileUploadSuccess3(file){
	$("#vedioUrl").val(file);
}
function delImg(wel){
	$(wel).parent().parent().remove();
}
function delOption(wel){
	$(wel).parent().remove();
}
function setImageParams(){
	var imagePathAll="";
	var imageNameAll="";
	var imageWidthAll="";
	var imageHeightAll="";
	var imagePath=$("#imgsShow .imagePath");
	var imageName=$("#imgsShow .imageName");
	var imageWidth=$("#imgsShow .imageWidth");
	var imageHeight=$("#imgsShow .imageHeight");
	for(var i=0;i<imagePath.length;i++){
		imagePathAll+=imagePath[i].value+";";
		imageNameAll+=imageName[i].value+";";
		imageWidthAll+=imageWidth[i].value+";";
		imageHeightAll+=imageHeight[i].value+";";
	}
	$("#imagePathAll").val(imagePathAll);
	$("#imageNameAll").val(imageNameAll);
	$("#imageWidthAll").val(imageWidthAll);
	$("#imageHeightAll").val(imageHeightAll);
}
function setOptionParams(){
	var optionPathAll="";
	var optionNameAll="";
	var optionPath=$("#optionList .optionPath");
	var optionName=$("#optionList .optionName");
	for(var i=0;i<optionPath.length;i++){
		optionPathAll+=optionPath[i].value+";";
		optionNameAll+=optionName[i].value+";";
	}
	$("#optionPathAll").val(optionPathAll);
	$("#optionNameAll").val(optionNameAll);
}
</script>
</head>
<body>
    <form:form action="${path}/admin/content/addOrUpdate" id="form_add_update" name="form_add_update" modelAttribute="pageForm">
	    <form:hidden path="id" />
	    <input type="hidden" id="imagePathAll" name="imagePathAll" />
	    <input type="hidden" id="imageNameAll" name="imageNameAll" />
	    <input type="hidden" id="imageWidthAll" name="imageWidthAll" />
	    <input type="hidden" id="imageHeightAll" name="imageHeightAll" />
	    <input type="hidden" id="optionPathAll" name="optionPathAll" />
	    <input type="hidden" id="optionNameAll" name="optionNameAll" />
	    <table class="formtable">
					<tr>
					    <td width="10%">信息标题</td>
						<td><form:input path="title" cssClass="dfinput" datatype="required"  errormsg="不能为空"/></td>
						<td>信息短标题</td>
						<td><form:input path="shortTitle" cssClass="dfinput" datatype="required"  errormsg="不能为空"/></td>
						<td>标题颜色</td>
						<td><form:input path="titleColor" cssClass="dfinput" datatype="required"  errormsg="不能为空"/></td>
					</tr>
					<tr>
					    <td width="10%">Tag标签</td>
						<td><form:input path="tags" cssClass="dfinput" datatype="required"  errormsg="不能为空"/></td>
						<td>来源</td>
						<td><form:input path="source" cssClass="dfinput" datatype="required"  errormsg="不能为空"/></td>
						<td>作者</td>
						<td><form:input path="author" cssClass="dfinput" datatype="required"  errormsg="不能为空"/></td>
					</tr>
					<tr>
						<td>缩略图</td>
						<td>
						   <form:input path="imgUrl" readonly="true" cssClass="dfinput"/>
						   <a href="javascript:fileUpload('fileUploadSuccess');" class="add">图片上传</a>
						   <span id="imgShow" style="position: absolute;z-index: 9999;display: block;width: 150px;height: 150px;right:230px;top:120px;">
						       <img src='${pageForm.imgUrl }' width="150" height="150" />
						   </span>
						</td>
						<td>编辑者</td>
						<td><form:input path="createUser" cssClass="dfinput" readonly="true"/></td>
						<td>创建时间</td>
						<td><form:input path="createTime"  cssClass="dfinput" readonly="true"/></td>
					</tr>
					<tr>
						<td>置顶</td>
						<td><form:radiobuttons items="${IS_TOP_TYPE_LIST }" path="isTop" itemValue="value" itemLabel="label" /></td>
						<td>允许评论</td>
						<td><form:radiobuttons items="${IS_COMMENT_TYPE_LIST }" path="isComment" itemValue="value" itemLabel="label" /></td>
					</tr>
					<tr>
						 <td>开始时间</td>
			             <td><form:input path="startTime" cssClass="dfinput" onclick="WdatePicker()"/></td>
			             <td>结束时间</td>
			             <td><form:input path="endTime" cssClass="dfinput" onclick="WdatePicker()"/></td>
					</tr>
					<tr>
						<td>所属栏目</td>
						<td>
						<input type="text" id="channelName" name="channelName"  readonly value="${channel.name }" class="dfinput"   datatype="required"  errormsg="不能为空" />
						<input type="hidden" id="channelId" name="channelId" value="${channel.id }" class="dfinput" />
						</td>
						<td>模型</td>
						<td>
						<input type="text" id="modelName" name="modelName"  readonly value="${model.name }" class="dfinput" datatype="required"  errormsg="不能为空" />
						<input type="hidden" id="modelId" name="modelId" value="${model.id }" class="dfinput"  />
						</td>
					</tr>
					<tr>
						<td>权重</td>
			             <td colspan="5"><form:hidden path="weight" cssClass="slider-input" /></td>
					</tr>
					<tr>
					   <td>模板</td>
						<td>
						    <form:select path="template" items="${templates }" itemLabel="label" itemValue="value"></form:select>
						</td>
					</tr>
					<tr>
					    <td>简介</td>
						<td colspan="5">
		                   <form:textarea path="description" cssClass="textinput"  />
						</td>
					</tr>
					<c:if test="${model.hasContent==hasContent }">
					<tr>
					    <td>内容</td>
						<td colspan="8">
		                   <script id="contentText" name="contentText" type="text/plain">${contentText}</script>
						</td>
					</tr> 
					</c:if>
					<c:if test="${model.hasGroupImages==hasGroupImages }">
					<tr>
					    <td>组图</td>
						<td colspan="8">
						   <div style="width: 100%;border: 1px solid #CED9DF;overflow: hidden;" id="imgsShow">
						       <div style="margin-left: 10px;"><a href="javascript:fileUpload('fileUploadSuccess1');" class="add">图片上传</a></div>
						       <c:forEach items="${contentImages }" var="contentImage">
						       <div style="width: 50%;height: 170px;float: left;">
			                     <div style="float: left;"><img src='${contentImage.imageUrl }' width="150" height="150" style="margin: 10px" /></div>
				    			 <div style="float: left;">
					   			  图片地址:<input type="text" class="dfinput imagePath" name="imagePath" readonly value="${contentImage.imageUrl }" /><br/>
					   			  图片描述:<input type="text" class="dfinput imageName" name="imageName" value="${contentImage.title }"  datatype="required"  errormsg="不能为空" /><br/>
					   			  图片宽度:<input type="text" class="dfinput imageWidth" name="imageWidth" value="150"  datatype="required"  errormsg="不能为空" /><br/>
					   			  图片高度:<input type="text" class="dfinput imageHeight" name="imageHeight" value="150"  datatype="required"  errormsg="不能为空" /><br/>
					     		 <a href="javascript:void(0);" onclick="delImg(this)" class="delete">删除</a>
				                 </div>
			                  </div>
			                  </c:forEach>
						   </div>
						</td>
					</tr>
					</c:if>
					<c:if test="${model.hasOptions==hasOptions }">
					<tr>
					    <td>附件列表</td>
						<td colspan="8">
						   <div style="width: 100%;border: 1px solid #CED9DF;overflow: hidden;" id="optionList">
						       <div style="margin-left: 10px;"><a href="javascript:fileUpload('fileUploadSuccess2');" class="add">附件上传</a></div>
						       <c:forEach items="${contentOptions }" var="contentOption">
						          <div style="margin: 10px">
			                                                                                      附件描述:<input type="text" class="dfinput optionName" name="optionName" value="${contentOption.title }" datatype="required"  errormsg="不能为空"  style="margin:0 30px 0 10px;" />
					                                                            附件地址:<input type="text" class="dfinput optionPath" name="optionPath" value="${contentOption.optionUrl }" readonly  style="margin:0 20px 0 10px;width:200px;" />
					                  <a href="javascript:void(0);" onclick="delOption(this)" class="delete">删除</a>
				                 </div>
						       </c:forEach>
						   </div>
						</td>
					</tr>
					</c:if>
					<c:if test="${model.hasVedio==hasVedio }">
					<tr>
					    <td>视屏</td>
		                <td colspan="8">
		                   <input type="text" id="vedioUrl" name="vedioUrl" class="dfinput" value="${vedioUrl }" />
						   <a href="javascript:fileUpload('fileUploadSuccess3');" class="add">视频上传</a>
						</td>
					</tr>
					</c:if>
		</table>
     </form:form>
     <jsp:include page="/view/admin/content/channelTree.jsp"/>
</body>
</html>
