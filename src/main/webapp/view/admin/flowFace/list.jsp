<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
</head>
<body>
    <form:form action="#" id="listForm" name="listForm">
    <jsp:include page="/view/admin/common/place.jsp" />
    <div class="rightinfo">
    <div class="tools">
    	<ul class="toolbar">
	        <li onclick="showForm('用户添加','${path}/admin/flowFace/showForm','750','350')"><span><i class="Hui-iconfont">&#xe600;</i></span>添加</li>
        </ul>
    </div>
    <table class="tablelist">
    	<thead>
	    	<tr>
	        <th><input type="checkbox" checked="checked"/></th>
	        <th>编号</th>
	        <th>工作流组</th>
	        <th>节点名称</th>
	        <th>介绍</th>
	        <th>排序</th>
         <th>操作</th>	        </tr>
       </thead>
        <tbody>
        <c:forEach items="${pageView.pageDate }" var="bean">
	        <tr>
	        <td><input name="id" type="checkbox" value="" /></td>
	        <td>${bean.id }</td>
	        <td>${bean.flow.name }</td>
	        <td>${bean.name }</td>
	        <td>${bean.description }</td>
	        <td>${bean.sort }</td>
	        <td class="td-manage">
		        <a title="编辑" href="javascript:showForm('用户修改','${path}/admin/flowFace/showForm?id=${bean.id }','750','350')" class="ml-5" style="text-decoration: none">
		            <i class="Hui-iconfont">&#xe6df;</i>
		        </a>
		        <a title="删除" href="javascript:del('${path }/admin/flowFace/delete?id=${bean.id}')" class="ml-5" style="text-decoration: none">
		            <i class="Hui-iconfont">&#xe6e2;</i>
		        </a>
			</td>
	        </tr>
       </c:forEach>
        </tbody>
    </table>
   <jsp:include page="/view/admin/common/pageinfo.jsp" />
    </div>
    </form:form>
</body>
</html>
