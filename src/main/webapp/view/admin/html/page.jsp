<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<%@ taglib prefix="fckeditor" uri="http://java.fckeditor.net" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
function channelCreate(){
	if($("#channelIds_1").val()){
		$.ajax({  
	        type : "POST",
	        url : path+"/admin/html/channel",
	        data : {"channelIds":$("#channelIds_1").val()},
	        dataType: 'json',
	        success : function(result) {
	        	if(result.success){
	        		layer.alert(result.message,{ icon: 4});
	        	}else{
	        		layer.alert(result.message,{ icon: 4});
	        	}
	        }  
	    });
	}else{
		layer.alert("请选择栏目",{ icon: 2});	
	}
	
}
function contentCreate(){
	if($("#channelIds_2").val()){
		$.ajax({  
	        type : "POST",
	        url : path+"/admin/html/content",
	        data : {"channelIds":$("#channelIds_2").val()},
	        dataType: 'json',
	        success : function(result) {
	        	if(result.success){
	        		layer.alert(result.message,{ icon: 4});
	        	}else{
	        		layer.alert(result.message,{ icon: 4});
	        	}
	        }  
	    });
	}else{
		layer.alert("请选择栏目",{ icon: 2});	
	}
}
</script>
</head>
<body>
    <form:form action="${path}/admin/html/channelCreate" id="theform" name="theform">
         <input type="hidden" id="modelIds" name="modelIds"/>
	     <table class="formtable" style="float: left;margin-top: 10px;margin-left: 20px;">
	                <tr>
					    <td>栏目静态化</td>
						<td>
						   <input type="text" id="channelNames_1" name="channelNames"  class="dfinput"  onclick="showMenu('channelNames_1','channelIds_1');" datatype="required"  errormsg="不能为空" />
						   <input type="hidden" id="channelIds_1" name="channelIds" class="dfinput"/>
						</td>
						<td><a href="javascript:channelCreate();" class="add">静态化</a></td>
					</tr>
					<tr>
					    <td>内容页静态化</td>
						<td>
						   <input type="text" id="channelNames_2" name="channelNames"  class="dfinput"  onclick="showMenu('channelNames_2','channelIds_2');" datatype="required"  errormsg="不能为空" />
						   <input type="hidden" id="channelIds_2" name="channelIds" class="dfinput"/>
						</td>
						<td><a href="javascript:contentCreate();" class="add">静态化</a></td>
					</tr>
		</table>
     </form:form>
     <jsp:include page="/view/admin/html/channelTree.jsp"/>
</body>
</html>
