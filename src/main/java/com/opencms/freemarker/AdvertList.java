package com.opencms.freemarker;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.opencms.bean.Advert;
import com.opencms.bean.AdvertPosition;
import com.opencms.freemarker.utils.Freemarker;
import com.opencms.service.AdvertPositionService;
import com.opencms.service.AdvertService;
import com.opencms.vo.QueryResult;

import freemarker.core.Environment;
import freemarker.ext.beans.ArrayModel;
import freemarker.ext.beans.BeanModel;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
/**
 * 广告列表
 * @author fumiao-pc
 *
 */
@Repository
public class AdvertList implements TemplateDirectiveModel {
	
	@Autowired
	private AdvertService advertService;
	
	@Autowired
	private AdvertPositionService advertPositionService;

	@Override
	public void execute(Environment env, Map map, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		Integer advertPositionId=Freemarker.getInteger(map, "advertPositionId");
		
		Integer size=Freemarker.getInteger(map, "size");
		Advert advert=new Advert();
		advert.setAdvertPositionId(advertPositionId);
		advert.setPageRow(size);
		QueryResult<Advert> queryResult=advertService.list(advert);
		AdvertPosition advertPosition=advertPositionService.selectByPrimaryKey(advertPositionId);
		loopVars[0]=new ArrayModel(queryResult.getQueryResult().toArray(),new BeansWrapper()); 
		loopVars[1]=new BeanModel(advertPosition,new BeansWrapper()); 
		body.render(env.getOut()); 
	}

}
