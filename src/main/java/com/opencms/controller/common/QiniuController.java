package com.opencms.controller.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.opencms.utils.qiniu.AuthException;
import com.opencms.utils.qiniu.Uptoken;

/**
 * 文章Controller
 * 
 * @author ThinkGem
 * @version 2013-3-23
 */
@Controller
@RequestMapping(value = "/common/qiniu/")
public class QiniuController {
	
	
    @RequestMapping(value = "getToken")
    @ResponseBody
    public String findate(HttpServletRequest request) {
    	String dir=request.getParameter("dir");
    	String token="";
		try {
			token = Uptoken.makeUptoken(dir);
		} catch (AuthException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
        return token;
    }
    
    @RequestMapping(value = "callback")
    public ModelAndView callback(HttpServletRequest request,
            HttpServletResponse response, Model model) {
    	ModelAndView mnv = new ModelAndView();
        mnv.setViewName("view/common/qiniu/qiNiuCallback");
    	return mnv;
    }
    
    @RequestMapping(value = "manager")
    public ModelAndView manager(HttpServletRequest request,
            HttpServletResponse response, Model model) {
    	ModelAndView mnv = new ModelAndView();
        mnv.setViewName("view/common/qiniu/imageManager");
    	return mnv;
    }
    



}
