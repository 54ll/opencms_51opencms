package com.opencms.controller.admin;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.opencms.bean.ContentReptile;
import com.opencms.controller.base.AdminBaseController;
import com.opencms.service.ContentReptileService;
import com.opencms.vo.ActionResult;
import com.opencms.vo.QueryResult;
@Controller
@RequestMapping("/admin/contentReptile/")
@SessionAttributes("pageForm")
public class ContentReptileController extends AdminBaseController{

     @Autowired
     private ContentReptileService contentReptileService;
     
     @RequestMapping("list")
     public ModelAndView list(ContentReptile contentReptile,HttpServletRequest request) throws Exception{
    	QueryResult<ContentReptile> queryResult=contentReptileService.list(contentReptile);
    	contentReptile.setPageDate(queryResult.getQueryResult());
    	contentReptile.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,contentReptile);
        mnv.setViewName("view/admin/contentReptile/list");
        return mnv;
     }
     
     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(ContentReptile contentReptile,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
    	   contentReptileService.deleteByPrimaryKey(contentReptile.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }
     
	 @RequestMapping("showForm")
	 public ModelAndView addForm(ContentReptile contentReptile,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		if(contentReptile.getId()!=null){
		   contentReptile=contentReptileService.selectByPrimaryKey(contentReptile.getId());
		}
		mnv.addObject(DEFAULT_PAGE_FORM, contentReptile);
		mnv.setViewName("view/admin/contentReptile/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(ContentReptile contentReptile,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			contentReptileService.saveOrUpdate(contentReptile);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
	
	@RequestMapping("toContent")
	@ResponseBody
	public ActionResult toContent(HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			String ids=request.getParameter("ids");
			String channelId=request.getParameter("channelId");
			contentReptileService.toContent(ids,channelId);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}


 
}
