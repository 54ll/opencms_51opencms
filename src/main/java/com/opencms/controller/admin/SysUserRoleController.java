package com.opencms.controller.admin;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.opencms.bean.SysUserRole;
import com.opencms.controller.base.AdminBaseController;
import com.opencms.service.SysUserRoleService;
import com.opencms.vo.ActionResult;
import com.opencms.vo.QueryResult;
@Controller
@RequestMapping("/admin/sysUserRole/")
@SessionAttributes("pageForm")
public class SysUserRoleController extends AdminBaseController{

     @Autowired
     private SysUserRoleService sysUserRoleService;

     @RequestMapping("list")
     public ModelAndView list(SysUserRole sysUserRole,HttpServletRequest request) throws Exception{
        QueryResult<SysUserRole> queryResult=sysUserRoleService.list(sysUserRole);
        sysUserRole.setPageDate(queryResult.getQueryResult());
        sysUserRole.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,sysUserRole);
        mnv.setViewName("view/admin/sysUserRole/list");
        return mnv;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(SysUserRole sysUserRole,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
	     sysUserRoleService.deleteByPrimaryKey(sysUserRole.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(SysUserRole sysUserRole,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		if(sysUserRole.getId()!=null){
			sysUserRole=sysUserRoleService.selectByPrimaryKey(sysUserRole.getId());
		}
		mnv.addObject(DEFAULT_PAGE_FORM, sysUserRole);
		mnv.setViewName("view/admin/sysUserRole/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(SysUserRole sysUserRole,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			sysUserRoleService.saveOrUpdate(sysUserRole);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
}
