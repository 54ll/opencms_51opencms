package com.opencms.controller.admin;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.opencms.annotation.PassCheckAnnotation;
import com.opencms.controller.base.AdminBaseController;
import com.opencms.vo.Source;
@Controller
@RequestMapping("/admin/document/")
@SessionAttributes("pageForm")
public class DocumentController extends AdminBaseController{
	
	 private  List<Source> sources=null;
	
	 @RequestMapping("dataPage")
	 @PassCheckAnnotation
     public ModelAndView dataPage(Source source,HttpServletRequest request) throws Exception{
       ModelAndView mnv = new ModelAndView();
       mnv.addObject("source", source);
       mnv.setViewName("view/admin/document/dataPage");
       return mnv;
    }

     @RequestMapping("fileTree")
     @ResponseBody
     @PassCheckAnnotation
     public List<Source> list(Source source,HttpServletRequest request) throws Exception{
    	 String documentPath=context.getRealPath("/template"); 
    	 if(source.getDocumentType()==1){
    		 documentPath=context.getRealPath("/template"); 
    	 }else{
    		 documentPath=context.getRealPath("/static"); 
    	 }
    	 
    	 sources=new ArrayList<Source>();
		 return readfile(documentPath,null);
     }
     
	@RequestMapping("show")
	@PassCheckAnnotation
     public ModelAndView show(Source source,HttpServletRequest request) throws Exception{
    	  File file=new File(source.getPath());
    	  String fileCode="";
    	  if(!file.isDirectory()) {
    		  BufferedReader reader = null;
    	      reader = new BufferedReader(new InputStreamReader(new FileInputStream(file),"utf-8"));
    	      String tempString = "";
    	      while ((tempString = reader.readLine()) != null) {
    	    	       fileCode+=tempString+"\r\n";
    	            }
    	       reader.close();
    	  }
    	  ModelAndView mnv = new ModelAndView();
    	  mnv.addObject("fileCode", StringEscapeUtils.escapeHtml(fileCode));
          mnv.setViewName("view/admin/document/form");
          return mnv;
     }
     
     public List<Source> readfile(String filepath,Integer pid) throws FileNotFoundException, IOException {
         try {
                 File file = new File(filepath);
                 if (!file.isDirectory()) {
                	 Source source=new Source();
                	 source.setId(sources.size()+1);
                	 source.setName(file.getName());
                	 source.setpId(null);
                	 source.setPath(filepath+File.separator+file.getName());
                	 sources.add(source);

                 } else if (file.isDirectory()) {
	                	 Source source=new Source();
	                	 source.setId(sources.size()+1);
	                	 source.setName(file.getName());
	                	 source.setpId(pid);
	                	 source.setPath(filepath);
	                	 source.setOpen(false);
	                	 sources.add(source);
                         String[] filelist = file.list();
                         for (int i = 0; i < filelist.length; i++) {
                                 File readfile = new File(filepath + File.separator + filelist[i]);
                                 if (!readfile.isDirectory()) {
                                    	 Source fileSource=new Source();
                                    	 fileSource.setId(sources.size()+1);
                                    	 fileSource.setName(readfile.getName());
                                    	 fileSource.setpId(source.getId());
                                    	 fileSource.setPath(filepath+File.separator+readfile.getName());
                                    	 sources.add(fileSource);

                                 } else if (readfile.isDirectory()) {
                                         readfile(filepath + File.separator + filelist[i],source.getId());
                                 }
                         }
                 }

         } catch (FileNotFoundException e) {
                 System.out.println("readfile()   Exception:" + e.getMessage());
         }
         return sources;
    }

}
