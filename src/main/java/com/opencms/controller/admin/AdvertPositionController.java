package com.opencms.controller.admin;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.opencms.bean.AdvertPosition;
import com.opencms.controller.base.AdminBaseController;
import com.opencms.service.AdvertPositionService;
import com.opencms.utils.DateUtil;
import com.opencms.utils.ListTypeParameter;
import com.opencms.vo.ActionResult;
import com.opencms.vo.QueryResult;
@Controller
@RequestMapping("/admin/advertPosition/")
@SessionAttributes("pageForm")
public class AdvertPositionController extends AdminBaseController{

     @Autowired
     private AdvertPositionService advertPositionService;

     @RequestMapping("list")
     public ModelAndView list(AdvertPosition advertPosition,HttpServletRequest request) throws Exception{
        QueryResult<AdvertPosition> queryResult=advertPositionService.list(advertPosition);
        advertPosition.setPageDate(queryResult.getQueryResult());
        advertPosition.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,advertPosition);
        mnv.setViewName("view/admin/advertPosition/list");
        return mnv;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(AdvertPosition advertPosition,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
	     advertPositionService.deleteByPrimaryKey(advertPosition.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(AdvertPosition advertPosition,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		if(advertPosition.getId()!=null){
			advertPosition=advertPositionService.selectByPrimaryKey(advertPosition.getId());
		}else{
			advertPosition.setCreateTime(DateUtil.getTodayTimeString());
		}
		mnv.addObject(DEFAULT_PAGE_FORM, advertPosition);
		mnv.addObject("ADVERT_POSITION_TYPE_LIST", ListTypeParameter.advertPositionTypeList);
		mnv.addObject("IS_OPEN", ListTypeParameter.isOpen);
		mnv.setViewName("view/admin/advertPosition/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(AdvertPosition advertPosition,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			advertPositionService.saveOrUpdate(advertPosition);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
}
