package com.opencms.controller.admin;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.opencms.annotation.PassCheckAnnotation;
import com.opencms.bean.Channel;
import com.opencms.bean.ChannelModel;
import com.opencms.bean.Model;
import com.opencms.controller.base.AdminBaseController;
import com.opencms.service.ChannelModelService;
import com.opencms.service.ChannelService;
import com.opencms.service.ModelService;
import com.opencms.utils.Constants;
import com.opencms.vo.ActionResult;
import com.opencms.vo.LabelValue;
import com.opencms.vo.QueryResult;
@Controller
@RequestMapping("/admin/channel/")
@SessionAttributes("pageForm")
public class ChannelController extends AdminBaseController{

     @Autowired
     private ChannelService channelService;
     
     @Autowired
     private ChannelModelService channelModelService;
     
     @Autowired
     private ModelService modelService;
     /**
      * 动态树
      */
     @RequestMapping("nodes")
     @ResponseBody
     public List<Channel> nodes(Channel channel,HttpServletRequest request) throws Exception{
    	 String parent_channel_id=request.getParameter("parent_channel_id");
    	 QueryResult<Channel> queryResult=null;
    	 LinkedHashMap<String, String> orderBy=new LinkedHashMap<String, String>();
     	 orderBy.put("sort", "asc");
    	 if(StringUtils.isBlank(parent_channel_id)){
    		 String whereSql=" (parentChannel=0 or parentChannel is null) ";
    		 queryResult=channelService.list(channel);
    		 for(int i=0;i<queryResult.getQueryResult().size();i++){
    			 Channel channel2=queryResult.getQueryResult().get(i);
    			 /*if(channel2.getChildrenChannel().size()>0){
    				 channel2.setIsParent(true);
    			 }else{
    				 channel2.setIsParent(false);
    			 }*/   			 
    		 }
    	 }else{
    		 channel=channelService.selectByPrimaryKey(Integer.valueOf(parent_channel_id));
    		 String whereSql=" parentChannel="+channel.getId()+" ";
    		 queryResult=channelService.list(channel);
    		 for(int i=0;i<queryResult.getQueryResult().size();i++){
    			 Channel channel2=queryResult.getQueryResult().get(i);
    			 /*if(channel2.getChildrenChannel().size()>0){
    				 channel2.setIsParent(true);
    			 }else{
    				 channel2.setIsParent(false);
    			 }*/
    		 }
    	 }
        return queryResult.getQueryResult();
     }
     
     @RequestMapping("simpleNodes")
     @ResponseBody
     @PassCheckAnnotation
     public List<Channel> simpleNodes(Channel channel,HttpServletRequest request) throws Exception{
    	 LinkedHashMap<String, String> orderBy=new LinkedHashMap<String, String>();
     	 orderBy.put("sort", "asc");
    	 QueryResult<Channel> queryResult=channelService.list();
    		 for(int i=0;i<queryResult.getQueryResult().size();i++){
    			 Channel channel2=queryResult.getQueryResult().get(i);
    			 if(channel2.getpId()==null){
    				 channel2.setOpen(true);
    			 }
    		 }
        return queryResult.getQueryResult();
     }
     
     @RequestMapping("list")
     public ModelAndView list(Channel channel,HttpServletRequest request) throws Exception{
    	QueryResult<Channel> queryResult=channelService.list(channel);
    	channel.setPageDate(queryResult.getQueryResult());
    	channel.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,channel);
        mnv.setViewName("view/admin/channel/list");
        return mnv;
     }

	@RequestMapping("dataPage")
     public ModelAndView dataPage(Channel channel,HttpServletRequest request) throws Exception{
        ModelAndView mnv = new ModelAndView();
        mnv.setViewName("view/admin/channel/dataPage");
        return mnv;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(Channel channel,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
    	 channelService.deleteByPrimaryKey(channel.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(Channel channel,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		if(channel.getId()!=null){
			channel=channelService.selectByPrimaryKey(channel.getId());
		}else{
			channel.setIsNav(2);
			channel.setIsOut(2);
		}
		QueryResult<Model> queryResult=modelService.list();
		Map<String, Object> params=new HashMap<String, Object>();
		QueryResult<ChannelModel> queryResult_1=new QueryResult<ChannelModel>();
		if(channel.getId()!=null){
			params.put("channelId", channel.getId());
			queryResult_1=channelModelService.list(params);
		}
		mnv.addObject(DEFAULT_PAGE_FORM, channel);
		mnv.addObject("modelList", queryResult.getQueryResult());
		mnv.addObject("channelModelList", queryResult_1.getQueryResult());
		mnv.addObject("templates", getTemplate());
		mnv.setViewName("view/admin/channel/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(Channel channel,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			String modelIds=request.getParameter("modelIds");
			channelService.saveOrUpdate(channel,modelIds);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
	
	@SuppressWarnings("unused")
	private void isParent(Channel channel) {
		  String whereSql=" parentChannel="+channel.getId()+" ";
		  QueryResult<Channel> queryResult=channelService.list();
		  if(queryResult.getCount()>0){
			  channel.setIsParent(true);
		  }
	  }
	
	
	private List<LabelValue> getTemplate(){
		String templatePath=context.getRealPath("/template/"+Constants.TEMPLATE_FOLDER+"/channel/");
		List<LabelValue> list=new ArrayList<LabelValue>();
		File file=new File(templatePath);
		String[] templates=file.list();
		for(int i=0;i<templates.length;i++){
			LabelValue labelValue=new LabelValue();
			labelValue.setLabel(Constants.TEMPLATE_FOLDER+"/channel/"+templates[i]);
			labelValue.setValue(Constants.TEMPLATE_FOLDER+"/channel/"+templates[i]);
			list.add(labelValue);
		}
		return list;
	}
}
