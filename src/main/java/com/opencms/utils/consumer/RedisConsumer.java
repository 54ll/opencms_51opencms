package com.opencms.utils.consumer;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.opencms.utils.command.BaseCommand;
import com.opencms.utils.redis.RedisShardClient;


/**
 * @author fumiao
 * @version 1.0
 */
public class RedisConsumer implements Runnable
{
	private final static Logger LOGGER= LoggerFactory.getLogger(RedisConsumer.class);
	private final static String REDIS_KEY="LIST_MESSAGE";
	
	private ThreadPoolTaskExecutor taskExecutor;
    private RedisShardClient redisShardClient;
    private Map<String, BaseCommand> baseCommands;
    
	public RedisConsumer() {
		super();
	}

	@Override
	public void run() {
		while (true) {
			List<String> list=redisShardClient.brpop(30,REDIS_KEY);
			if(list!=null){
			  String message=list.get(1);
			  try {
				    LOGGER.info(message);
				    taskExecutor.execute(new Message(message));
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
		}
	}
	
	class Message implements Runnable{
		String message="";
		public Message(String message) {
			this.message=message;
		}
		@Override
		public void run() {
			try {
				JSONObject jsonObject = new JSONObject(message);
	        	String commandName=(String) jsonObject.get("commandName");
	        	JSONObject params=jsonObject.getJSONObject("params");
	        	BaseCommand baseCommand=baseCommands.get(commandName);
	        	if(baseCommand!=null){
	        		baseCommand.execute(params.toString());
	        	}else{
	        		LOGGER.error("not fond command");
	        	}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public ThreadPoolTaskExecutor getTaskExecutor() {
		return taskExecutor;
	}

	public void setTaskExecutor(ThreadPoolTaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}

	public RedisShardClient getRedisShardClient() {
		return redisShardClient;
	}

	public void setRedisShardClient(RedisShardClient redisShardClient) {
		this.redisShardClient = redisShardClient;
	}

	public Map<String, BaseCommand> getBaseCommands() {
		return baseCommands;
	}

	public void setBaseCommands(Map<String, BaseCommand> baseCommands) {
		this.baseCommands = baseCommands;
	}

}
