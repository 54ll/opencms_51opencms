package com.opencms.utils.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;

import com.opencms.utils.DictUtil;


public class ShowCnContent extends TagSupport{
	
	
	private String id;
	
	public int doStartTag() throws JspException {
		String text="功能";
		if(StringUtils.isNotBlank(id)){
			text=DictUtil.getNameById(Integer.valueOf(id));
		}
		JspWriter out = pageContext.getOut(); 
		try {
			out.write(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


}
