package com.opencms.bean;

import com.opencms.vo.PageView;

public class SysUserRole extends PageView<SysUserRole> {
    private Integer id;

    private Integer roleId;

    private Integer sysUserId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(Integer sysUserId) {
        this.sysUserId = sysUserId;
    }
}