package com.opencms.bean;

import com.opencms.vo.PageView;

public class Model extends PageView<Model> {
    private Integer id;

    private String name;

    private String tempPrefix;

    private Integer hasGroupImages;

    private Integer hasVedio;

    private Integer hasContent;

    private Integer hasOptions;

    private String createTime;

    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getTempPrefix() {
        return tempPrefix;
    }

    public void setTempPrefix(String tempPrefix) {
        this.tempPrefix = tempPrefix == null ? null : tempPrefix.trim();
    }

    public Integer getHasGroupImages() {
        return hasGroupImages;
    }

    public void setHasGroupImages(Integer hasGroupImages) {
        this.hasGroupImages = hasGroupImages;
    }

    public Integer getHasVedio() {
        return hasVedio;
    }

    public void setHasVedio(Integer hasVedio) {
        this.hasVedio = hasVedio;
    }

    public Integer getHasContent() {
        return hasContent;
    }

    public void setHasContent(Integer hasContent) {
        this.hasContent = hasContent;
    }

    public Integer getHasOptions() {
        return hasOptions;
    }

    public void setHasOptions(Integer hasOptions) {
        this.hasOptions = hasOptions;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
}