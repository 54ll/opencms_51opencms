package com.opencms.dao;

import com.opencms.bean.Flow;
import com.opencms.dao.base.MapperSupport;

public interface FlowMapper extends MapperSupport<Flow> {
}