package com.opencms.dao;

import com.opencms.bean.ContentOption;
import com.opencms.dao.base.MapperSupport;

public interface ContentOptionMapper  extends MapperSupport<ContentOption> {
}