package com.opencms.dao;

import com.opencms.bean.ContentText;
import com.opencms.dao.base.MapperSupport;

public interface ContentTextMapper extends MapperSupport<ContentText> {
}