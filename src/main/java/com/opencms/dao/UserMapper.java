package com.opencms.dao;

import com.opencms.bean.User;
import com.opencms.dao.base.MapperSupport;

public interface UserMapper extends MapperSupport<User> {
}