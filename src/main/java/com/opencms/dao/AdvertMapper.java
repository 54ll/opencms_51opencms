package com.opencms.dao;

import com.opencms.bean.Advert;
import com.opencms.dao.base.MapperSupport;

public interface AdvertMapper extends MapperSupport<Advert> {
    
}