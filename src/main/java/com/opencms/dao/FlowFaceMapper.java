package com.opencms.dao;

import com.opencms.bean.FlowFace;
import com.opencms.dao.base.MapperSupport;

public interface FlowFaceMapper extends MapperSupport<FlowFace> {
}