package com.opencms.dao;

import com.opencms.bean.ContentImage;
import com.opencms.dao.base.MapperSupport;

public interface ContentImageMapper extends MapperSupport<ContentImage> {
}