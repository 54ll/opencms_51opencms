package com.opencms.dao;

import com.opencms.bean.Dict;
import com.opencms.dao.base.MapperSupport;

public interface DictMapper extends MapperSupport<Dict> {
}