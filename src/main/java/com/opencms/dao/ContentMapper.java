package com.opencms.dao;

import com.opencms.bean.Content;
import com.opencms.dao.base.MapperSupport;

public interface ContentMapper extends MapperSupport<Content> {
}