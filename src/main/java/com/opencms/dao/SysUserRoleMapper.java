package com.opencms.dao;

import com.opencms.bean.SysUserRole;
import com.opencms.dao.base.MapperSupport;

public interface SysUserRoleMapper extends MapperSupport<SysUserRole> {

	
}