package com.opencms.dao;

import com.opencms.bean.ChannelModel;
import com.opencms.dao.base.MapperSupport;

public interface ChannelModelMapper extends MapperSupport<ChannelModel> {
}