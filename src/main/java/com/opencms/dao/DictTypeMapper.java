package com.opencms.dao;

import com.opencms.bean.DictType;
import com.opencms.dao.base.MapperSupport;

public interface DictTypeMapper extends MapperSupport<DictType> {
}