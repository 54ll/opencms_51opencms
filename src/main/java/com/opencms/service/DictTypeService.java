package com.opencms.service;
import com.opencms.bean.DictType;
import com.opencms.service.base.ServiceSupport;
public interface DictTypeService extends ServiceSupport<DictType> {
}
