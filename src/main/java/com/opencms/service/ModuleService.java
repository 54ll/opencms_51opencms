package com.opencms.service;
import com.opencms.bean.Module;
import com.opencms.service.base.ServiceSupport;
public interface ModuleService extends ServiceSupport<Module> {
}
