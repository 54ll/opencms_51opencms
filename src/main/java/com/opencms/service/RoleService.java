package com.opencms.service;
import com.opencms.bean.Role;
import com.opencms.service.base.ServiceSupport;
public interface RoleService extends ServiceSupport<Role> {
}
