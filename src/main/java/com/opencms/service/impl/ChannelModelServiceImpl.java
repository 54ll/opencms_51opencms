package com.opencms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.ChannelModel;
import com.opencms.dao.ChannelModelMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.ChannelModelService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class ChannelModelServiceImpl extends ServiceSupportImpl<ChannelModel> implements ChannelModelService {

	@Autowired
	private ChannelModelMapper channelModelMapper;

	@Override
	public MapperSupport<ChannelModel> getMapperSupport() {
		return channelModelMapper;
	}
	
	
}
