package com.opencms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.User;
import com.opencms.dao.UserMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.UserService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class UserServiceImpl extends ServiceSupportImpl<User> implements UserService {

	@Autowired
	private UserMapper userMapper;

	@Override
	public MapperSupport<User> getMapperSupport() {
		return userMapper;
	}
	
	
}
