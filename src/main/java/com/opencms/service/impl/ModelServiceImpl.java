package com.opencms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.Model;
import com.opencms.dao.ModelMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.ModelService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class ModelServiceImpl extends ServiceSupportImpl<Model> implements ModelService {

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public MapperSupport<Model> getMapperSupport() {
		return modelMapper;
	}
	
	
}
