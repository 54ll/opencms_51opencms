package com.opencms.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.FriendLink;
import com.opencms.dao.FriendLinkMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.FriendLinkService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class FriendLinkServiceImpl extends ServiceSupportImpl<FriendLink> implements FriendLinkService {
   @Autowired
   private FriendLinkMapper friendLinkMapper;

@Override
public MapperSupport<FriendLink> getMapperSupport() {
	// TODO Auto-generated method stub
	return friendLinkMapper;
}
}
