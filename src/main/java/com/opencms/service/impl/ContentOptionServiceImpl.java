package com.opencms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.ContentOption;
import com.opencms.dao.ContentOptionMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.ContentOptionService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class ContentOptionServiceImpl extends ServiceSupportImpl<ContentOption> implements ContentOptionService {

	@Autowired
	private ContentOptionMapper contentOptionMapper;

	@Override
	public MapperSupport<ContentOption> getMapperSupport() {
		return contentOptionMapper;
	}
	
	
}
