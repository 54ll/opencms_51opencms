package com.opencms.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.Dict;
import com.opencms.dao.DictMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.DictService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class DictServiceImpl extends ServiceSupportImpl<Dict> implements DictService {
   @Autowired
   private DictMapper dictMapper;

@Override
public MapperSupport<Dict> getMapperSupport() {
	// TODO Auto-generated method stub
	return dictMapper;
}
   
}
