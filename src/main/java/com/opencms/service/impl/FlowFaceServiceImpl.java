package com.opencms.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.FlowFace;
import com.opencms.dao.FlowFaceMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.FlowFaceService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class FlowFaceServiceImpl extends ServiceSupportImpl<FlowFace> implements FlowFaceService {
   @Autowired
   private FlowFaceMapper flowFaceMapper;

@Override
public MapperSupport<FlowFace> getMapperSupport() {
	// TODO Auto-generated method stub
	return flowFaceMapper;
}
}
