package com.opencms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.ReptileRule;
import com.opencms.dao.ReptileRuleMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.ReptileRuleService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class ReptileRuleServiceImpl extends ServiceSupportImpl<ReptileRule> implements ReptileRuleService {

	@Autowired
	private ReptileRuleMapper reptileRuleMapper;

	@Override
	public MapperSupport<ReptileRule> getMapperSupport() {
		return reptileRuleMapper;
	}
	
	
}
