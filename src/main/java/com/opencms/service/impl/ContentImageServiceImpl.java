package com.opencms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.ContentImage;
import com.opencms.dao.ContentImageMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.ContentImageService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class ContentImageServiceImpl extends ServiceSupportImpl<ContentImage> implements ContentImageService {

	@Autowired
	private ContentImageMapper contentImageMapper;

	@Override
	public MapperSupport<ContentImage> getMapperSupport() {
		return contentImageMapper;
	}
	
	
}
