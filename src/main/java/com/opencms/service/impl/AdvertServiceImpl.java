package com.opencms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.Advert;
import com.opencms.dao.AdvertMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.AdvertService;
import com.opencms.service.base.impl.ServiceSupportImpl;

@Service
public class AdvertServiceImpl extends ServiceSupportImpl<Advert>implements AdvertService {
	@Autowired
	private AdvertMapper advertMapper;

	@Override
	public MapperSupport<Advert> getMapperSupport() {
		return advertMapper;
	}
}
