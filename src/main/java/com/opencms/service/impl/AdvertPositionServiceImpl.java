package com.opencms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.AdvertPosition;
import com.opencms.dao.AdvertPositionMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.AdvertPositionService;
import com.opencms.service.base.impl.ServiceSupportImpl;

@Service
public class AdvertPositionServiceImpl extends ServiceSupportImpl<AdvertPosition>implements AdvertPositionService {
	@Autowired
	private AdvertPositionMapper advertPositionMapper;

	@Override
	public MapperSupport<AdvertPosition> getMapperSupport() {
		return advertPositionMapper;
	}

}
