package com.opencms.service;
import com.opencms.bean.ContentVideo;
import com.opencms.service.base.ServiceSupport;
public interface ContentVideoService extends ServiceSupport<ContentVideo> {
}
