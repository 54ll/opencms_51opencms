package com.opencms.service;
import com.opencms.bean.ChannelModel;
import com.opencms.service.base.ServiceSupport;
public interface ChannelModelService extends ServiceSupport<ChannelModel> {
}
