package com.opencms.service;
import com.opencms.bean.FriendLink;
import com.opencms.service.base.ServiceSupport;
public interface FriendLinkService extends ServiceSupport<FriendLink> {
}
