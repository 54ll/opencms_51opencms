package com.opencms.service;
import javax.servlet.http.HttpServletRequest;

import com.opencms.bean.Content;
import com.opencms.service.base.ServiceSupport;
public interface ContentService extends ServiceSupport<Content> {
	
	void saveOrUpdate(Content content,HttpServletRequest request);
}
