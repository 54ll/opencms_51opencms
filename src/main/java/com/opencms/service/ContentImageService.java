package com.opencms.service;
import com.opencms.bean.ContentImage;
import com.opencms.service.base.ServiceSupport;
public interface ContentImageService extends ServiceSupport<ContentImage> {
}
