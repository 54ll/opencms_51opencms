package com.opencms.service;
import com.opencms.bean.Dict;
import com.opencms.service.base.ServiceSupport;
public interface DictService extends ServiceSupport<Dict> {
}
