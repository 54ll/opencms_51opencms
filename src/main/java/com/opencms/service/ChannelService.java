package com.opencms.service;
import com.opencms.bean.Channel;
import com.opencms.service.base.ServiceSupport;
public interface ChannelService extends ServiceSupport<Channel> {

	Channel selectByCode(String code);
	
	void saveOrUpdate(Channel channel,String modelIds);
}
